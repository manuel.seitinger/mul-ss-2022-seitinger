<xsl:stylesheet version="1.0"
  	xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

  <xsl:template match="/dictionary">
    <html>
      <head>
        <style>
            h1 {
              text-align:center;
            }

            table {
                border-collapse: collapse;
                width: 100%;
              }
              
              tr {
                border-bottom: 1px solid #ddd;
              }

              td {
                text-align:center;
              } 
              
              p {
                text-align:center;
              } 
              
        </style>
      </head>
      <body>
          <h1>Wörterliste</h1>
        <table>
            <tr>
                <th>Englisch</th>
                <th>Deutsch</th>
                <th>Kategorie</th>
            </tr>
            
            <xsl:for-each select="word">
              <xsl:sort select="@value" order="descending"></xsl:sort>
                <tr>
                 <td><xsl:value-of select="@value" /></td>
                 <td><xsl:value-of select="translation" /></td>
                 <xsl:choose>
                    <xsl:when test="category='Animal'">
                      <td>(&#129409;) <xsl:value-of select="category" /></td>
                    </xsl:when>
                    <xsl:when test="category='Geography'">
                      <td>(&#127758;) <xsl:value-of select="category" /></td>
                    </xsl:when>
                    <xsl:when test="category='Food'">
                      <td>(&#127789;) <xsl:value-of select="category" /></td>
                    </xsl:when>
                    <xsl:otherwise>
                        <td>(&#127960;) <xsl:value-of select="category" /></td>
                     </xsl:otherwise>
                  </xsl:choose>
                </tr>
            </xsl:for-each>
        </table>

        <h1>Statistik</h1>
        <xsl:if test="count(//word)>= 0">
          <p>Die Liste enthält <strong><xsl:value-of select="count(//word)" /> Vokabeln</strong></p>

          <p>Deutsch: <xsl:value-of select="count(//translation[@lang='DE'])" /></p>
          <p>Französisch: <xsl:value-of select="count(//translation[@lang='FR'])" /></p>
          <p>Latein: <xsl:value-of select="count(//translation[@lang='LA'])" /></p>  
        </xsl:if>

      </body>
    </html>
  </xsl:template>

</xsl:stylesheet>
