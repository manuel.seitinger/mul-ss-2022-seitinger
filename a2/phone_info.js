function getBrands(){
    const url="https://api-mobilespecs.azharimm.site/v2/brands"
    
    fetch(url)
    .then((response) => {
       return response.json()
    })
    .then((data) => {

        data.data.forEach(element => {
            const phone = document.createElement("option")
            phone.innerText = element.brand_name 
            phone.value = element.brand_slug
            document.getElementById("phones").appendChild(phone) 
            console.dir(phone)
        })

        
    })
    .catch((message) => {
        console.log(message)
    })
}


function createPhoneList(){
    const list = document.createElement("ul")
    list.setAttribute('id',"phones_list")
    document.getElementById("content").appendChild(list)
}

function search(){

    if(document.getElementById("content").innerHTML = ""){
        createPhoneList()
    }else{
        document.getElementById("content").innerHTML = ""
        createPhoneList()
    }

    const phones = document.getElementById("phones")
    const url="https://api-mobilespecs.azharimm.site/v2/brands/" + phones.value;
    fetch(url)
    .then((response) => {
       return response.json()
    })
    .then((data) => {
        data.data.phones.forEach(element => {
            const phone = document.createElement("li")
            phone.innerText = element.phone_name
            phone.setAttribute('onclick',"showDetails('"+element.slug+"')")

            document.getElementById("phones_list").appendChild(phone)
        })
    })
    .catch((message) => {
        console.log(message)
    })
}


function createViewPhoneDetails(data){
    document.getElementById("content").innerHTML = ""
    const phoneSpecs = document.createElement("div")
    phoneSpecs.classList.add("phone-details")
    const brand = document.createElement("p")
    const model = document.createElement("p")
    const operatingSystem = document.createElement("p")
    const releaseDate = document.createElement("p")
    const phoneImage = document.createElement("img")
    brand.innerText = "Marke: " + data.data.brand
    model.innerText = "Modell: " + data.data.phone_name
    operatingSystem.innerText = "Betriebssystem: " + data.data.os
    releaseDate.innerText = "Erscheinungsdatum: " + data.data.release_date
    phoneImage.setAttribute('src',data.data.thumbnail)
    phoneSpecs.appendChild(brand)
    phoneSpecs.appendChild(model)
    phoneSpecs.appendChild(operatingSystem)
    phoneSpecs.appendChild(releaseDate)
    phoneSpecs.appendChild(phoneImage)
    document.getElementById("content").appendChild(phoneSpecs)
}


function showDetails(slug){
    const url="https://api-mobilespecs.azharimm.site/v2/" + slug;
    fetch(url)
    .then((response) => {
       return response.json()
    })
    .then((data) => {
        createViewPhoneDetails(data)
    })
    .catch((message) => {
        console.log(message)
    })
}
